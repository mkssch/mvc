<!DOCTYPE HTML>
<html lang='ru'>
    <head>
        <title><?=$title?></title>
        <meta http-equiv="CONTENT-TYPE" content="text/html" charset="utf-8"/>
        <meta name="description" content="<?=$meta_desc?>"/>
        <meta name="keywords" content="<?=$meta_keywords?>"/>
    </head>
    <body>
        <header>
            <h1>Добро пожаловать!</h1>
        </header>
        <hr/>
        <div id="content"><?=$content?></div>
        <hr/>
        <footer>
            С уважением, администрация.
        </footer>
    </body>
</html>