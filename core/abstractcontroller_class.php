<?php
/**
 * Created by PhpStorm.
 * User: Kostya
 * Date: 028 28.11.17
 * Time: 22:23
 */

abstract class AbstractController{
    protected $view;

    public function __construct($view)
    {
        $this->view = $view;
    }

    public function Action404(){
        header('HTTP/1.1 404 Not found');
        header('Status: 404 Not found');
    }

    abstract protected function Render($content);

}