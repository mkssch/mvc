<?php
/**
 * Created by PhpStorm.
 * User: Kostya
 * Date: 028 28.11.17
 * Time: 22:32
 */

class Route{
    public static function Start(){
        $controller_name = 'Main';
        $action_name = 'index';

        $uri = $_SERVER['REQUEST_URI'];
        $uri = str_replace('/MVC_Patern/','', $uri);
        if($uri)
            $action_name = $uri;
        $controller_name = $controller_name.'Controller';
        $action_name = 'Action'.$action_name;
        var_dump($action_name);
        $controller = new $controller_name;
        if(method_exists($controller, $action_name))
            $controller->$action_name();
        else
            $controller->Action404();
    }
}