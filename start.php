<?php
/**
 * Created by PhpStorm.
 * User: Kostya
 * Date: 028 28.11.17
 * Time: 23:28
 */

set_include_path(get_include_path().PATH_SEPARATOR.'core'.PATH_SEPARATOR.'controllers');
spl_autoload_extensions('_class.php');
spl_autoload_register();

define('DIR_TMPL', __DIR__.DIRECTORY_SEPARATOR.'tmpl'.DIRECTORY_SEPARATOR);
define('MAIN_LAYOUT', 'Main');
