<?php
/**
 * Created by PhpStorm.
 * User: Kostya
 * Date: 028 28.11.17
 * Time: 22:39
 */
class MainController extends AbstractController {
    protected $title;
    protected $meta_desc;
    protected $meta_keywords;

    public function __construct()
    {
        parent::__construct(new View(DIR_TMPL));
    }

    public function Action404(){
        parent::Action404();
        $this->title = 'Страница не найдена 404';
        $this->meta_desc = 'Запрашиваемая страница не существует';
        $this->meta_keywords = 'Страница не найдена, 404';

        $content = $this->view->Render('404', [], true);
        $this->Render($content);
    }

    public function ActionIndex(){
        $this->title = 'Главная страница';
        $this->meta_desc = 'Описание главной страницы';
        $this->meta_keywords = 'Описание, главная страница';

        $content = $this->view->Render('Index', [], true);
        $this->Render($content);
    }

    public function ActionPage(){
        $this->title = 'Внутренняя страница';
        $this->meta_desc = 'Описание внутренней страницы';
        $this->meta_keywords = 'Описание, внутренняя страница';

        $content = $this->view->Render('Page', [], true);
        $this->Render($content);
    }

    protected function Render($content)
    {
        $params = [];
        $params['title'] = $this->title;
        $params['meta_desc'] = $this->meta_desc;
        $params['meta_keywords'] = $this->meta_keywords;
        $params['content'] = $content;

        $this->view->Render(MAIN_LAYOUT, $params);
    }
}